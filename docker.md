# docker log 調整為 journald

###### step1: 更改 docker 預設储存配置

```
> sudo vim /etc/docker/daemon.js
{
    "log-driver": "journald"
}
```

###### step2: 調整 journald 配置預設值, 讓 journald 不會塞爆 systemlog

```
> sudo vim /etc/systemd/journald.conf
```

* ForwardToSyslog=no
* ForwardToWall=no

###### step3: 重啟docker and journald service

```
sudo systemctl restart docker
sudo systemctl restart systemd-journald.service
```

[reference](http://docs.lvrui.io/2017/02/19/%E6%9B%B4%E6%94%B9docker%E7%9A%84%E6%97%A5%E5%BF%97%E5%BC%95%E6%93%8E%E4%B8%BA-journald/)

# 常用指令

###### container exec command

```
docker exec -ti container cmd
``` 

###### contaier copy file to host

```
docker cp container:file host-path
```

###### host copy file to contaier

```
docker cp host-path container:file 
```

##### docker logs export

```
journalctl CONTAINER_NAME=name --since "YYYY-MM-DD HH:MM:SS"  --until "YYYY-MM-DD HH:MM:SS"
journalctl CONTAINER_NAME=name --since yesterday

```

##### docker get all containers ip 

```
docker inspect -f '{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq)
```


##### docker export/import image 

```
docker save IMG_NAME > IMG_NAME.tar

docker load < IMG_NAME.tar
```
