
# 常用指令

###### 強制把 memory data 寫回 file

```
$ redis-cli -h $host -p 6379 save
```


###### 確認 redis key list

```
$ redis-cli -h $host -p 6379 keys *
```

###### 取得 redis key type

```
$ redis-cli -h $host -p 6379 type $key
```


###### 取得 redis value by key

for string 

```
$ redis-cli -h $host -p 6379 get $keys
```

for hash
```
$ redis-cli -h $host -p 6379 hgetall $keys
```


for list
```
$ redis-cli -h $host -p 6379 lrange $keys
```

for set
```
$ redis-cli -h $host -p 6379 semebers $keys
```

for zset
```
$ redis-cli -h $host -p 6379 zrange $keys 0 -1 withscores
```

