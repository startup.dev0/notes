# 常用指令

###### 使用帳密登入

```
$ mongo -u user -p password
```

###### dump db 

```
$ mongodump -u user -p password  -d db-name -o filepath
```

若上述指令出現  error: unable to authenticate using mechanism "SCRAM-SHA-1": (AuthenticationFailed) Authentication failed. 則加入以下參數

```
$ mongodump -u user -p password  -d db-name -o filepath --authenticationDatabase admin
```

###### 回寫 restore db

```
$ mongorestore -u user -p password -d db-name -drop file-path --authenticationDatabase admin
```


###### dump collection

```
$ mongodump -u user -p password  -d db-name -c collection -o filepath --authenticationDatabase admin
```


###### 回寫 collection

```
$ mongorestore -u user -p password  -d db-name -c collection  file-path.bson --authenticationDatabase admin
```


# docker-compose example username and password can save .env file

```
version: "3.7"

#networks:

services:
  mongo:
    image: "mongo:4.4.8"
    container_name: mongo
    environment:
      - MONGO_INITDB_ROOT_USERNAME=${USERNAME}
      - MONGO_INITDB_ROOT_PASSWORD=${PASSWORD}
      - MONGO_INITDB_DATABASE="admin"
    restart: always
    #volumes:
    ports:
      - "27017:27017"
    #networks:

```
 
# docker-compose 含 Transaction 功能 example, 需有 replica set 

key file 產生指令

```
openssl rand -base64 756 >  keyfile
chmod 400 keyfile
```


```

version: "3.7"

services:
  mongors:
    container_name: mongors
    image: mongo:4.4.8
    volumes:
      - ./database:/data/db
      - ./keyfile:/home/keyfile
      #- ./mongod.conf:/etc/mongod.conf
    environment:
      - MONGO_INITDB_ROOT_USERNAME=${USERNAME}
      - MONGO_INITDB_ROOT_PASSWORD=${PASSWORD}
      - MONGO_INITDB_DATABASE="admin"
    ports:
      - 27017:27017
    entrypoint: ["mongod", "--port", "27017",  "--bind_ip_all",   "--replSet", "rs0", "--keyFile", "/home/keyfile", "--auth"]
    restart: always


```

啟動後一定要做 initicate 動作

```
# mongo
> rs.initicate()
```



# mongo 開啟 replica 後，docker 刪除重建後，如何將原有的資料求回

```
Step1: 關閉 auth 功能後，刪除 db name local 

# mongo
> use local
> db.dropDatabase()

Step2: 重新啟動 含有 replica mongo service

# mongo
> use admin
> db.auth('xxx', 'xxx')
> rs.initial()   
``` 

Step4: 加入其它節點

‵``
rs.add('xxxx')         // replica node
rs.add('xxxx', true)   // arbiter node
```

