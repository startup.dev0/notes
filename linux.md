
# 常用指令

######  壓縮/解壓縮

```
.tar
打包：tar cvf FileName.tar DirName
解包： tar xvf FileName.tar

.gz
壓縮：gzip FileName
解壓1：gunzip FileName.gz
解壓2：gzip -d FileName.gz

.tar.gz
壓縮：tar zcvf FileName.tar.gz DirName
解壓：tar zxvf FileName.tar.gz

.zip
壓縮：zip FileName.zip DirName
解壓：unzip FileName.zip

.rar
壓縮：rar e FileName.rar
解壓：rar a FileName.rar

```

##### 憑證驗證方式

```
openssl x509 -noout -modulus -in server.pem | openssl md5
openssl rsa -noout -modulus -in server.key | openssl md5
```
